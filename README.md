# AGESCI palette

`agesci.css` is meant to be used standalone or in combination with Bootstrap.

`palette.css` contains color variables.

It's available a palette file to be used in the Adobe CS/CC (`AGESCI.ase`)

`agesci.css` contains `.text-`, `.border-` and `.bg-` which set respectivly text, border and background colour.

The classes are made appenfing the following colour names to the prefixes (`.text-`, `.border-` and `.bg-`)

![palette](palette.png)

_Source: [AGESCI](https://www.agesci.it/download/ufficiostampa/immagine_coordinata_agesci/Manuale-Immagine_Coordinata_2016.pdf)_
